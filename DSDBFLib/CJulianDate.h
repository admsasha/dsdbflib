#pragma once
#ifndef _JULIANDATE_H_
#define _JULIANDATE_H_
 
#include <iostream>
#include <cstdlib>
#include <math.h>
#include <vector>
 
#define PRINT_ERROR(msg) std::cout   << "ERROR: '" << msg << "' IN FILE: " << __FILE__ \
                << ":" << __LINE__ << std::endl; exit(EXIT_FAILURE);
 
 
class CJulianDate
{
public:
    CJulianDate() { }
    ~CJulianDate() { }
 
    // Перевод обычной даты в юлианскую
    double DateToJD(std::vector<int> date);
    double DateToJD( int day ,int month, int year);
 
    //Перевод Юлианского дня в Юлианскую дату
    std::vector<int> JDToJulian(double juliandate);
 
    //Перевод Юлианской даты в Юлианскую день
    double JulianToJD(int day, int month, int year);
 
    // Перевод Юлианского дня в Григорианского дату
    std::vector<int> JDToDate(double juliandate);
 
private:
    // Проверка даты
    void check_date(int day, int month, int year);
};
#endif
