#include "DSDBFLib.h"

#include <iostream>
#include <iomanip>
#include <string.h>
#include <tuple>
#include <sys/stat.h>
#include <time.h>
#include <fstream>

#include "DSDBFUtil.h"

// https://msdn.microsoft.com/en-us/library/aa975386(v=vs.71).aspx


namespace DSDBFLib {

DSDBFLib::DSDBFLib(const std::string &filename) : m_filename(filename){
    codepages = {
        {0x01 ,std::make_tuple(437,"US MS-DOS")},
        {0x02 ,std::make_tuple(850,"International MS-DOS")},
        {0x03 ,std::make_tuple(1252,"Windows ANSI Latin I")},
        {0x04 ,std::make_tuple(10000,"Standard Macintosh")},
        {0x08 ,std::make_tuple(865,"Danish OEM")},
        {0x09 ,std::make_tuple(437,"Dutch OEM")},
        {0x0A ,std::make_tuple(850,"Dutch OEM*")},
        {0x0B ,std::make_tuple(437,"Finnish OEM")},
        {0x0D ,std::make_tuple(437,"French OEM")},
        {0x0E ,std::make_tuple(850,"French OEM*")},
        {0x0F ,std::make_tuple(437,"German OEM")},
        {0x10 ,std::make_tuple(850,"German OEM*")},
        {0x11 ,std::make_tuple(437,"Italian OEM")},
        {0x12 ,std::make_tuple(850,"Italian OEM*")},
        {0x13 ,std::make_tuple(932,"Japanese Shift-JIS")},
        {0x14 ,std::make_tuple(850,"Spanish OEM*")},
        {0x15 ,std::make_tuple(437,"Swedish OEM")},
        {0x16 ,std::make_tuple(850,"Swedish OEM*")},
        {0x17 ,std::make_tuple(865,"Norwegian OEM")},
        {0x18 ,std::make_tuple(437,"Spanish OEM")},
        {0x19 ,std::make_tuple(437,"English OEM (Great Britain)")},
        {0x1A ,std::make_tuple(850,"English OEM (Great Britain)*")},
        {0x1B ,std::make_tuple(437,"English OEM (US)")},
        {0x1C ,std::make_tuple(863,"French OEM (Canada)")},
        {0x1D ,std::make_tuple(850,"French OEM*")},
        {0x1F ,std::make_tuple(852,"Czech OEM")},
        {0x22 ,std::make_tuple(852,"Hungarian OEM")},
        {0x23 ,std::make_tuple(852,"Polish OEM")},
        {0x24 ,std::make_tuple(860,"Portuguese OEM")},
        {0x25 ,std::make_tuple(850,"Portuguese OEM*")},
        {0x26 ,std::make_tuple(866,"Russian OEM")},
        {0x37 ,std::make_tuple(850,"English OEM (US)*")},
        {0x40 ,std::make_tuple(852,"Romanian OEM")},
        {0x4D ,std::make_tuple(936,"Chinese GBK (PRC)")},
        {0x4E ,std::make_tuple(949,"Korean (ANSI/OEM)")},
        {0x4F ,std::make_tuple(950,"Chinese Big5 (Taiwan)")},
        {0x50 ,std::make_tuple(874,"Thai (ANSI/OEM)")},
        {0x58 ,std::make_tuple(1252,"Western European ANSI")},
        {0x59 ,std::make_tuple(1252,"Spanish ANSI")},
        {0x64 ,std::make_tuple(852,"Eastern European MS-DOS")},
        {0x65 ,std::make_tuple(866,"Russian MS-DOS")},
        {0x66 ,std::make_tuple(865,"Nordic MS-DOS")},
        {0x67 ,std::make_tuple(861,"Icelandic MS-DOS")},
        {0x68 ,std::make_tuple(895,"Kamenicky (Czech) MS-DOS")},
        {0x69 ,std::make_tuple(620,"Mazovia (Polish) MS-DOS")},
        {0x6A ,std::make_tuple(737,"Greek MS-DOS (437G)")},
        {0x6B ,std::make_tuple(857,"Turkish MS-DOS")},
        {0x6C ,std::make_tuple(863,"French-Canadian MS-DOS")},
        {0x78 ,std::make_tuple(950,"Taiwan Big 5")},
        {0x79 ,std::make_tuple(949,"Hangul (Wansung)")},
        {0x7A ,std::make_tuple(936,"PRC GBK")},
        {0x7B ,std::make_tuple(932,"Japanese Shift-JIS")},
        {0x7C ,std::make_tuple(874,"Thai Windows/MS–DOS")},
        {0x86 ,std::make_tuple(737,"Greek OEM")},
        {0x87 ,std::make_tuple(852,"Slovenian OEM")},
        {0x88 ,std::make_tuple(857,"Turkish OEM")},
        {0x96 ,std::make_tuple(10007,"Russian Macintosh")},
        {0x97 ,std::make_tuple(10029,"Eastern European Macintosh")},
        {0x98 ,std::make_tuple(10006,"Greek Macintosh")},
        {0xC8 ,std::make_tuple(1250,"Eastern European Windows")},
        {0xC9 ,std::make_tuple(1251,"Russian Windows")},
        {0xCA ,std::make_tuple(1254,"Turkish Windows")},
        {0xCB ,std::make_tuple(1253,"Greek Windows")},
        {0xCC ,std::make_tuple(1257,"Baltic Windows")},
    };

    filetypes = {
        {0x02,"FoxBASE"},
        {0x03,"FoxBASE+/Dbase III plus, no memo"},
        {0x30,"Visual FoxPro"},
        {0x31,"Visual FoxPro, autoincrement enabled"},
        {0x32,"Visual FoxPro with field type Varchar or Varbinary"},
        {0x43,"dBASE IV SQL table files, no memo"},
        {0x63,"dBASE IV SQL system files, no memo"},
        {0x83,"FoxBASE+/dBASE III PLUS, with memo"},
        {0x8B,"dBASE IV with memo"},
        {0x8C,"dBASE 7"},
        {0xCB,"dBASE IV SQL table files, with memo"},
        {0xF5,"FoxPro 2.x (or earlier) with memo"},
        {0xE5,"HiPer-Six format with SMT memo file"},
        {0xFB,"FoxBASE"},
    };
}

void DSDBFLib::setDBFile(const std::string &filename){
    m_filename=filename;
}

bool DSDBFLib::open(){
    dbFieldList.clear();

    m_fileHeader.open (m_filename, std::ifstream::in | std::ifstream::binary);
    if (!m_fileHeader.is_open()) return false;

    m_fileHeader.read ((char*)&m_dbHeader,32);


//    std::cout << "sign: 0x" << std::hex << std::setw(2) << std::setfill('0') << int(m_dbHeader.signature) << std::endl;
//    std::cout << "day: " << std::dec << m_dbHeader.lastUpdateDay << std::endl;
//    std::cout << "mon: " << std::dec << m_dbHeader.lastUpdateMonth << std::endl;
//    std::cout << "year: " << std::dec << m_dbHeader.lastUpdateYear << std::endl;
//    std::cout << "records: " << std::dec << m_dbHeader.records << std::endl;
//    std::cout << "lengthHeader: " << std::dec << m_dbHeader.lengthHeader << std::endl;
//    std::cout << "lengthRecord: " << std::dec << m_dbHeader.lengthRecord << std::endl;
//    std::cout << "codepage: 0x" << std::hex << std::setw(2) << std::setfill('0') << int(m_dbHeader.codepage) << std::endl;
//    std::cout << "flagIndMdx: 0x" << std::hex << std::setw(2) << std::setfill('0') << int(m_dbHeader.flagIndMdx) << std::endl;
//    std::cout << std::endl;


    struct stat filestat;
    std::string pathFptFile =m_filename.substr(0,m_filename.length()-4)+".fpt";
    if (!stat(pathFptFile.c_str(), &filestat)){
        if (!fpt.open(pathFptFile)){
            return false;
        }
    }

    std::string pathDbtFile =m_filename.substr(0,m_filename.length()-4)+".dbt";
    if (!stat(pathDbtFile.c_str(), &filestat)){
        if (!dbt.open(pathDbtFile)){
            return false;
        }
    }

    // dBASE 7 пока не поддерживается
    if (m_dbHeader.signature==0x8c) return false;

    // Чтение полей
    m_fileHeader.seekg(32);

    char buffer[32];
    dbField field;
    while (true) {
        int pos = m_fileHeader.tellg();
        buffer[0]=m_fileHeader.get();
        if (buffer[0]==0x0d) break;
        m_fileHeader.seekg(pos);

        m_fileHeader.read ((char*)&field,32);
        DSDBFUtil::normalizeLengthField((FIELD_TYPE)field.type,&field.length,&field.numberOfDecimalPlaces);
        dbFieldList.push_back(field);
    }


    return true;
}

void DSDBFLib::close(){
    fpt.close();
    dbt.close();
    m_fileHeader.close();
}

uint8_t DSDBFLib::getFileType(){
    return m_dbHeader.signature;
}

std::string DSDBFLib::getFileTypeStr(){
    if (filetypes.count(m_dbHeader.signature)==0) return "unknow";
    return filetypes[m_dbHeader.signature];
}

std::string DSDBFLib::getTimeLastUpdate(){
    int year = 0;
    int mon = m_dbHeader.lastUpdateMonth;
    int day = m_dbHeader.lastUpdateDay ;

    if (m_dbHeader.lastUpdateYear<50){
        year = 2000 + m_dbHeader.lastUpdateYear;
    } else if (m_dbHeader.lastUpdateYear<201){
        year = 1900 + m_dbHeader.lastUpdateYear;
    }else{
        year = 1792 + m_dbHeader.lastUpdateYear;
    }

    char date[11];
    sprintf(date,"%02d.%02d.%i",day,mon,year);

    return std::string(date,10);
}

int DSDBFLib::getLengthHeader(){
    return m_dbHeader.lengthHeader;
}

int DSDBFLib::getCodepage(){
    return std::get<0>(codepages[m_dbHeader.codepage]);
}

bool DSDBFLib::setFileType(uint8_t type){
    m_fileHeader.close();

    std::ofstream ofs;
    ofs.open (m_filename, std::ios_base::in|std::ios_base::binary);
    if (!ofs.is_open()) return false;

    ofs.seekp(0,std::ios::beg);
    ofs.put (type);
    ofs.close();

    open();
    return true;
}

bool DSDBFLib::setTimeLastUpdate(int day, int month, int year){
    m_fileHeader.close();

    std::ofstream ofs;
    ofs.open (m_filename, std::ios_base::in|std::ios_base::binary);
    if (!ofs.is_open()) return false;

    ofs.seekp(1,std::ios::beg);
    if (year>2000){
        ofs.put (year-2000);
    }else if (year>1900){
        ofs.put (year-1900);
    }else{
        ofs.put (year-1792);
    }

    ofs.seekp(2,std::ios::beg);
    ofs.put (month);

    ofs.seekp(3,std::ios::beg);
    ofs.put (day);


    ofs.close();

    open();
    return true;
}

bool DSDBFLib::setCodepage(int codepage){
    for (auto& x: codepages) {
        if (std::get<0>(x.second)==codepage){
            return setCodepage(x.first);
        }
    }

    return false;
}

bool DSDBFLib::setCodepage(uint8_t id){
    m_fileHeader.close();

    std::ofstream ofs;
    ofs.open (m_filename, std::ios_base::in|std::ios_base::binary);
    if (!ofs.is_open()) return false;

    ofs.seekp(29,std::ios::beg);
    ofs.put (id);

    ofs.close();

    open();
    return true;
}


size_t DSDBFLib::getNumRecords(){
    return m_dbHeader.records;
}

size_t DSDBFLib::getNumFields(){
    return dbFieldList.size();
}

size_t DSDBFLib::getLengthRecord(){
    return m_dbHeader.lengthRecord;
}

std::vector<dbField> DSDBFLib::getFields(){
    return dbFieldList;
}


// Читаем строку
dbRecord DSDBFLib::getRecord(size_t number,TYPE_RESULT type_result){
    dbRecord result;

    if (!m_fileHeader.is_open()) return result;
    if (number>=m_dbHeader.records) return result;

    m_fileHeader.seekg(m_dbHeader.lengthHeader+number*m_dbHeader.lengthRecord);

    char type;
    type=m_fileHeader.get();
    result.type=type;

    for (dbField field: dbFieldList){
        int length=field.length;
        result.record[std::string(field.name)]="";

        char * buffer = new char [length+1];
        m_fileHeader.read (buffer,length);

        // Бинарный результат
        if (type_result==TYPE_RESULT::BINARY){
            result.record[std::string(field.name)]=std::string(buffer,length);
        }else{
           if (field.type==0x4d){  // M - Memo
                uint32_t value=0;
                for (int i=0;i<4;i++){
                    value <<=8LL;
                    value ^= (uint8_t)buffer[3-i];
                }
                if (value!=0){
                    result.record[std::string(field.name)]=fpt.readBlock(value);
                }
            }else if (field.type==0x50 or field.type==0x47 or field.type==0x42){  // P - picture, G, B
                int block = std::stoi(std::string(buffer,10));
                if (block!=0){
                    if (m_dbHeader.signature==0x8c){
                        result.record[std::string(field.name)]=dbt.readBlock(block);
                    }else{
                        result.record[std::string(field.name)]=fpt.readBlock(block);
                    }
                }
            }else{
                result.record[std::string(field.name)]=DSDBFUtil::convertBinaryToString(buffer,field);
            }
        }

        delete[] buffer;

    }

    return result;
}

bool DSDBFLib::createDbfFile(uint8_t fileType){
    time_t rawtime;
    struct tm * timeinfo;

    std::ofstream ofs;
    ofs.open (m_filename, std::ios_base::out|std::ios_base::binary);
    if (!ofs.is_open()) return false;

    char header[33];
    memset(header,0x00,32);

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    header[0]=fileType;

    header[1]=timeinfo->tm_year;
    header[2]=timeinfo->tm_mon+1;
    header[3]=timeinfo->tm_mday;

    header[8]=33;

    ofs.write(header,32);
    ofs.put(0x0D);
    ofs.put(0x1A);

    ofs.close();

    return true;
}

bool DSDBFLib::insertField(std::string name, FIELD_TYPE fieldType, uint8_t length,uint8_t numberOfDecimalPlaces){
    time_t rawtime;
    struct tm * timeinfo;

    if (m_fileHeader.is_open()) return false;

    // Нормализация длины полей
    DSDBFUtil::normalizeLengthField(fieldType,&length,&numberOfDecimalPlaces);

    // TODO: Вместо копирования заменить на переименование
    std::string tmpFilename = std::tmpnam(nullptr);
    DSDBFUtil::copyFile(m_filename,tmpFilename);

    // Открытие старой базы
    DSDBFLib oldDbf(tmpFilename);
    if (!oldDbf.open()) return false;
    uint8_t oldSign = oldDbf.getFileType();
    uint32_t oldLengthHeader = oldDbf.getLengthHeader();
    std::vector<dbField> fields = oldDbf.getFields();


    if ((oldSign==0x8C and name.length()>32) or (oldSign!=0x8C and name.length()>11)){
        oldDbf.close();
        remove(tmpFilename.c_str());
        return false;
    }

    // Создание нового файла
    std::ofstream ofs;
    ofs.open (m_filename, std::ios_base::out|std::ios_base::binary);
    if (!ofs.is_open()){
        oldDbf.close();
        remove(tmpFilename.c_str());
        return false;
    }

    char header[33];
    memset(header,0x00,32);

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    header[0]=oldSign;
    header[1]=timeinfo->tm_year;
    header[2]=timeinfo->tm_mon+1;
    header[3]=timeinfo->tm_mday;
    ofs.write(header,32);

    dbField newField;
    strncpy(newField.name,name.c_str(),name.length());
    newField.type=(uint8_t)fieldType;
    newField.length=length;
    newField.flagMDX=0;
    newField.reserve1=0;
    newField.reserve2[0]=0;
    newField.numberOfDecimalPlaces=numberOfDecimalPlaces;
    newField.nextAutoIncrement=0;
    fields.push_back(newField);

    int countLenField = 0;
    int countRecords = 0;


    // Пишем структуру
    for (dbField f:fields){
        ofs.write((char*)&f,32);
        countLenField+=f.length;
    }
    countLenField+=1;

    // Символы окончания структуры
    ofs.put(0x0D);
    ofs.put(0x1A);


    int addLenHead=0;
    // Добавляем блок для 0x30 (visual foxpro)
    if (oldSign==0x30){
        for (int x=0;x<263;x++) ofs.put(0x00);
        addLenHead=263;
    }


    // Пишем строки
    for (size_t i=0;i<oldDbf.getNumRecords();i++){
        dbRecord record = oldDbf.getRecord(i,TYPE_RESULT::BINARY);
        ofs.put(0x20);
        for (dbField field : fields){
            if (record.record.count(std::string(field.name))!=0){
                ofs.write(record.record[std::string(field.name)].data(),field.length);
            }else{
                char * buffer = new char [field.length+1];
                memset(buffer,0x20,field.length);
                ofs.write(buffer,field.length);
                delete[] buffer;
            }
        }
        countRecords++;
    }

    ofs.put(0x1A);


    // Пересчитываем заголовок
    uint16_t lHead = 32+fields.size()*32+2+addLenHead;
    DSDBFUtil::writeUint16(&ofs,0x08,lHead);

    ofs.seekp(0x04);
    ofs.put(countRecords);

    ofs.seekp(0x0A);
    DSDBFUtil::writeUint16(&ofs,0x0A,countLenField);


    ofs.close();



    oldDbf.close();
    remove(tmpFilename.c_str());
    return true;
}

bool DSDBFLib::insertRecord(dbRecord record){
    DSDBFLib oldDbf(m_filename);
    if (!oldDbf.open()) return false;

    uint8_t sign = oldDbf.getFileType();
    uint32_t lengthHeader = oldDbf.getLengthHeader();
    std::vector<dbField> fields = oldDbf.getFields();
    int countRecords = oldDbf.getNumRecords();
    int lengthRecord = oldDbf.getLengthRecord();
    oldDbf.close();

    // открытие файла
    std::ofstream ofs;
    ofs.open (m_filename, std::ios_base::in|std::ios_base::binary);
    if (!ofs.is_open()){
        return false;
    }

    ofs.seekp(lengthHeader+countRecords*lengthRecord);

    // Пишем строки
    ofs.put(0x20);
    for (dbField field : fields){  
        char * buffer = new char [field.length+1];
        DSDBFUtil::convertStringToBinary(buffer,record.record[std::string(field.name)],field);
        ofs.write(buffer,field.length);
        delete[] buffer;
    }
    countRecords++;

    ofs.put(0x1A);

    ofs.seekp(0x04);
    ofs.put(countRecords);


    ofs.close();

    return true;
}

bool DSDBFLib::editRecord(size_t number, dbRecord record){

    return true;
}

bool DSDBFLib::editRecord(size_t number, std::string fieldName, std::string value){

    return true;
}



}
