#ifndef DSDBFLIB_H
#define DSDBFLIB_H

#include <string>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <stdint.h>

#include "DSDBFLib.h"
#include "DSDBF_fpt.h"
#include "DSDBF_dbt.h"
#include "DSDBFStruct.h"

namespace DSDBFLib {

enum TYPE_RESULT {
    PLAIN,
    BINARY,
};

class DSDBFLib{
    public:
        explicit DSDBFLib(const std::string &filename);
        void setDBFile(const std::string &filename);

        // Reader
        bool open();
        void close();

        uint8_t getFileType();
        std::string getFileTypeStr();
        std::string getTimeLastUpdate();
        int getLengthHeader();
        int getCodepage();

        size_t getNumRecords();
        size_t getNumFields();
        size_t getLengthRecord();
        std::vector<dbField> getFields();

        dbRecord getRecord(size_t number, TYPE_RESULT type_result = TYPE_RESULT::PLAIN);


        // Editor
        bool createDbfFile(uint8_t fileType);
        bool insertField(std::string name, FIELD_TYPE fieldType, uint8_t length=0, uint8_t numberOfDecimalPlaces=0);
        bool insertRecord(dbRecord record);
        bool editRecord(size_t number,dbRecord record);
        bool editRecord(size_t number,std::string fieldName,std::string value);

        bool setFileType(uint8_t type);
        bool setTimeLastUpdate(int day,int month,int year);
        bool setCodepage(int codepage);
        bool setCodepage(uint8_t id);

    private:
        std::string m_filename;
        std::ifstream m_fileHeader;

        dbHeader m_dbHeader;
        std::vector<dbField> dbFieldList;

        std::map<uint8_t,std::tuple<int,std::string>> codepages;
        std::map<uint8_t,std::string> filetypes;

        DSDBF_fpt fpt;
        DSDBF_dbt dbt;

};


}

#endif // DSDBFLIB_H
