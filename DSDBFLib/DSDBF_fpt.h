#ifndef DSDBF_FPT_H
#define DSDBF_FPT_H

#include <string>
#include <fstream>

namespace DSDBFLib {

class DSDBF_fpt {
    public:
        DSDBF_fpt();

        bool open(const std::string &filename);
        void close();

        std::string readBlock(size_t block);

    private:
        std::string m_filename;
        std::ifstream m_fileHeader;

        int m_NextFreeBlock;
        int m_sizeBlock;

};

}

#endif // DSDBF_FPT_H
