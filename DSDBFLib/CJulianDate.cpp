#include "CJulianDate.h"
#include <iostream>
#include <math.h>

//----------------------------------------------------------------------------//
//                     Перевод обычной даты в Юлианский день                  //
//----------------------------------------------------------------------------//
double CJulianDate::DateToJD(std::vector<int> date)
{
    return DateToJD(date[0], date[1], date[2]);
}
//----------------------------------------------------------------------------//
//                      Перевод обычной даты в Юлианский день                 //
//----------------------------------------------------------------------------//
double CJulianDate::DateToJD(int day, int month, int year)
{
    double jd;
    check_date(day, month, year);
 
 
    double ggg = 1;
    if (year < 1582) { ggg = 0; }
    if (year <= 1582 && month < 10) { ggg = 0; }
    if (year <= 1582 && month == 10 && day < 5) { ggg = 0; }
    jd = -1 * (int)(7 * ((int)((month + 9) / 12) + year) / 4);
 
    int s = 1;
    if ((month - 9) < 0) { s = -1; }
    int a = month - 9;
    if (a < 0) { a *= -1; }
    double j1 = (int)(year + s * (int)(a / 7));
    j1 = -1 * (int)((floor(j1 / 100) + 1) * 3 / 4);
    jd = jd + (int)(275 * month / 9) + day + (ggg * j1);
    jd = jd + 1721027 + 2 * ggg + 367 * year - 0.5;
 
    return jd;
}
//----------------------------------------------------------------------------//
//                Перевод Юлианского дня в Юлианскую дату                    //
//----------------------------------------------------------------------------//
std::vector<int> CJulianDate::JDToJulian(double juliandate)
{
    double JD = juliandate;
    double  c, d, e;
    int day, month, year, z, a, b;
 
    JD += 0.5;
    z = floor(JD);
    a = z;
    b = a + 1524;
    c = floor((b - 122.1) / 365.25);
    d = floor(365.25 * c);
    e = floor((b - d) / 30.6001);
 
    month = floor((e < 14) ? (e - 1) : (e - 13));
    year = floor((month > 2) ? (c - 4716) : (c - 4715));
    day = b - d - floor(30.6001 * e);
 
    /*  If year is less than 1, subtract one to convert from
    a zero based date system to the common era system in
    which the year -1 (1 B.C.E) is followed by year 1 (1 C.E.).  */
 
    if (year < 1) {year--;}
 
    std::vector<int> date;
    date.push_back((int)day);
    date.push_back((int)month);
    date.push_back((int)year);
    return date;
}
//----------------------------------------------------------------------------//
//             Перевод Юлианской даты в Юлианскую день                        //
//----------------------------------------------------------------------------//
double CJulianDate::JulianToJD(int day, int month, int year)
{
    /* Adjust negative common era years to the zero-based notation we use.  */
    if (year < 1) 
    {
        year++;
    }   
    if (month <= 2) 
    {
        year--;
        month += 12;
    }
    return ((floor((365.25 * (year + 4716))) +
             floor((30.6001 * (month + 1))) +
                                     day) - 1524.5);
}
//----------------------------------------------------------------------------//
//           Перевод Юлианского дня в Григорианского дату                    //
//----------------------------------------------------------------------------//
std::vector<int> CJulianDate::JDToDate(double juliandate)
{
    double JD = juliandate;
 
    double A, I, B, C, D, T, RJ, day , RH;
    double month = 0;
    double year = 0;
    double Z = floor(JD + 0.5);
    double F = JD + 0.5 - Z;
    if (Z < 2299161) { A = Z; }
    else
    {
        I = floor((Z - 1867216.25) / 36524.25);
        A = Z + 1 + I - floor(I / 4);
    }
    B = A + 1524;
    C = floor((B - 122.1) / 365.25);
    D = floor(365.25 * C);
    T = floor((B - D) / 30.600);
    RJ = B - D - floor(30.6001 * T) + F;
    day = floor(RJ);
    RH = (RJ - floor(RJ)) * 24;
 
 
    if (T < 14) { month = T - 1; }
    else
    {
        if ((T == 14) || (T == 15)) { month = T - 13; }
    }
 
    if (month > 2) { year = C - 4716; }
    else
    {
        if ((month == 1) || (month == 2)) { year = C - 4715; }
    }
 
    std::vector<int> date;
    date.push_back((int)day);
    date.push_back((int)month);
    date.push_back((int)year);
 
    return date;
}
//----------------------------------------------------------------------------//
//                             Проверка даты                                  //
//----------------------------------------------------------------------------//
void CJulianDate::check_date(int day, int month, int year)
{
    if (month > 12 || month < 1) { PRINT_ERROR("ERROR: Month must be 1,2,3,...,12"); }
    if (day   > 31 || day   < 0) { PRINT_ERROR("Day > 31"); }
    system("pause");
}
