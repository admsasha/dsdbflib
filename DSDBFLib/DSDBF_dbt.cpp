#include "DSDBF_dbt.h"
#include <iostream>

namespace DSDBFLib {

DSDBF_dbt::DSDBF_dbt()
{

}

bool DSDBF_dbt::open(const std::string &filename){
    m_filename=filename;
    m_NextFreeBlock=0;

    m_fileHeader.open (m_filename, std::ifstream::in | std::ifstream::binary);
    if (!m_fileHeader.is_open()) return false;


//    {   // Расположение следующего свободного блока*
//        uint32_t value=0;
//        char buffer[5] ;
//        m_fileHeader.read (buffer,4);
//        for (int i=0;i<4;i++){
//            value <<=8LL;
//            value ^= (uint8_t)buffer[i];
//        }
//        m_NextFreeBlock=value;
//    }
//    m_fileHeader.seekg(20);
//    {   // Размер блока (число байтов в блоке)
//        uint16_t value=0;
//        char buffer[5] ;
//        m_fileHeader.read (buffer,2);
//        for (int i=0;i<2;i++){
//            value <<=8LL;
//            value ^= (uint8_t)buffer[i];
//        }
//        m_sizeBlock=value;
//    }

    m_sizeBlock=512;

    return true;
}

std::string DSDBF_dbt::readBlock(size_t block){
    if (!m_fileHeader.is_open()) return "";
    std::string result="";

    m_fileHeader.seekg(block*m_sizeBlock);

    int lenBlock=0;
    int signBlock=0;

    {   // Сигнатура блока
        uint32_t value=0;
        char buffer[5] ;
        m_fileHeader.read (buffer,4);
        for (int i=0;i<4;i++){
            value <<=8LL;
            value ^= (uint8_t)buffer[i];
        }
        signBlock=value;
    }
    {   // Длина* memo (в байтах)
        uint32_t value=0;
        char buffer[5] ;
        m_fileHeader.read (buffer,4);
        for (int i=0;i<4;i++){
            value <<=8LL;
            value ^= (uint8_t)buffer[3-i];
        }
        lenBlock=value-8;
    }

    char * buffer = new char [lenBlock+1];
    m_fileHeader.read (buffer,lenBlock);
    buffer[lenBlock]=0x00;
    result=std::string(buffer,lenBlock);
    delete[] buffer;

    return result;
}

void DSDBF_dbt::close(){
    m_fileHeader.close();
}


}
