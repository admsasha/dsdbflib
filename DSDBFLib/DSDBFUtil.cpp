#include "DSDBFUtil.h"
#include <fstream>
#include <memory.h>
#include <sstream>
#include <iostream>

#include "CJulianDate.h"

namespace DSDBFLib {

DSDBFUtil::DSDBFUtil()
{

}


void DSDBFUtil::copyFile(std::string filename_origin, std::string filename_destination){
    std::ifstream infile(filename_origin, std::ios::binary);
    std::ofstream outfile(filename_destination, std::ios::binary);
    outfile << infile.rdbuf();
    outfile.close();
    infile.close();
}


uint8_t DSDBFUtil::readUint8(std::ifstream *stream,size_t position){
    stream->seekg(position);

    char buffer;
    stream->get (buffer);

    return buffer;
}

uint16_t DSDBFUtil::readUint16(std::ifstream *stream, size_t position){
    uint32_t result=0;
    char buffer;

    for (unsigned int i=0;i<sizeof(uint16_t);i++){
        stream->seekg(position+(sizeof(uint16_t)-1-i));
        stream->get (buffer);
        result <<=8LL;
        result ^= (uint8_t)buffer;
    }

    return result;
}


uint32_t DSDBFUtil::readUint32(std::ifstream *stream, size_t position){
    uint32_t result=0;
    char buffer;

    for (unsigned int i=0;i<sizeof(uint32_t);i++){
        stream->seekg(position+(sizeof(uint32_t)-1-i));
        stream->get (buffer);
        result <<=8LL;
        result ^= (uint8_t)buffer;
    }

    return result;
}

void DSDBFUtil::writeUint16(std::ofstream *stream, size_t position, uint16_t value){
    stream->seekp(position);
    stream->put((value>>0) & 0xFF);
    stream->seekp(position+1);
    stream->put((value>>8) & 0xFF);
}

void DSDBFUtil::writeData(char *source, char *dest, int pos, int length){
    for (int i=0;i<length;i++){
        source[pos+i]=dest[i];
    }
}

bool DSDBFUtil::convertStringToBinary(char *dest, std::string str, dbField field){
    int startPos=0;
    std::stringstream ss;

    memset(dest,0x20,field.length);

    switch (FIELD_TYPE(field.type)) {
        case FIELD_TYPE::L:          // L - Logical
            if (str=="T" or str=="t" or str=="Y" or str=="y"){
                dest[0]='T';
            } else if (str=="F" or str=="f" or str=="N" or str=="n"){
                dest[0]='F';
            } else  if (str=="" or str=="?"){
                dest[0]=' ';
            } else {
                return false;
            }
            return true;
            break;
        case FIELD_TYPE::Y:          // Y - Currency
            try {
                long double number_float = std::stold(str)*10000.0;
                int64_t number = number_float;
                for (int i=0;i<8;i++){
                    dest[i] = (number>>(i*8)) & 0xFF;
                }
                return true;
            }
            catch (...){
                return false;
            }

            break;
        case FIELD_TYPE::F:          // F - Float	ASCII символы(-.0123456789)
        case FIELD_TYPE::N:          // N - Numeric	ASCII символы(-.0123456789)
            try {
                ss.precision(field.numberOfDecimalPlaces);
                ss << std::fixed << std::stod(str);
                str = ss.str();

                if (str.length()>field.length) str = str.substr(str.length()-field.length,field.length);

                startPos = field.length-str.length();
                for (int i=startPos;i<field.length;i++) dest[i]=str.at(i-startPos);
                return true;
            }
            catch (...){
                return false;
            }
            break;
        default:
            for (size_t i=0;i<str.length();i++) dest[i]=str.at(i);
            return true;
            break;
    }


    return false;
}

std::string DSDBFUtil::convertBinaryToString(char *source, dbField field){
    int n = 0;
    char buf[1024];
    int64_t currency=0;
    long double value=0;
    uint32_t valueUint32=0;
    uint32_t valueUint32P2=0;
    std::string format = "";
    std::vector<int> vectorDate;
    CJulianDate julianDate;


    switch (FIELD_TYPE(field.type)) {
        case FIELD_TYPE::AUTOINCREMENT:
            for (int i=0;i<4;i++){
                valueUint32 <<=8LL;
                valueUint32 ^= (uint8_t)source[3-i];
            }
            return std::to_string(valueUint32);
            break;

        case FIELD_TYPE::Y:
            for (int i=0;i<field.length;i++){
                currency <<=8LL;
                currency ^= (uint8_t)source[field.length-1-i];
            }
            value = (long double)currency/10000.00;
            format = "%."+std::to_string(field.numberOfDecimalPlaces)+"Lf";
            n=std::sprintf(buf, format.c_str(), value);
            return std::string(buf,n);
            break;

        case FIELD_TYPE::T:
        case FIELD_TYPE::TIMESTAMP:
            for (int i=0;i<4;i++){
                valueUint32 <<=8LL;
                valueUint32 ^= (uint8_t)source[3-i];
            }
            for (int i=0;i<4;i++){
                valueUint32P2 <<=8LL;
                valueUint32P2 ^= (uint8_t)source[7-i];
            }

            vectorDate = julianDate.JDToDate((double) valueUint32+(double) valueUint32P2/pow(10,sizeof(double)));

            sprintf(buf,"%02d.%02d.%i",vectorDate.at(0),vectorDate.at(1),vectorDate.at(2));

            return std::string(buf,10);

        case FIELD_TYPE::F:
            return std::string(source,field.length);
            break;

        default:
            return std::string(source,field.length);
            break;
    }

}

void DSDBFUtil::normalizeLengthField(FIELD_TYPE fieldType,uint8_t *length,uint8_t *numberOfDecimalPlaces){

    if (fieldType==FIELD_TYPE::B){
        *length=10;
        *numberOfDecimalPlaces=0;
    }
    if (fieldType==FIELD_TYPE::P){
        *length=10;
        *numberOfDecimalPlaces=0;
    }
    if (fieldType==FIELD_TYPE::G){
        *length=10;
        *numberOfDecimalPlaces=0;
    }
    if (fieldType==FIELD_TYPE::M){
        *length=4;
        *numberOfDecimalPlaces=0;
    }
    if (fieldType==FIELD_TYPE::L){
        *length=1;
        *numberOfDecimalPlaces=0;
    }
    if (fieldType==FIELD_TYPE::T){
        *length=8;
        *numberOfDecimalPlaces=0;
    }
    if (fieldType==FIELD_TYPE::TIMESTAMP){          // @
        *length=8;
        *numberOfDecimalPlaces=0;
    }
    if (fieldType==FIELD_TYPE::Y){
        *length=8;
        *numberOfDecimalPlaces=4;
    }
    if (fieldType==FIELD_TYPE::AUTOINCREMENT){      // +
        *length=4;
        *numberOfDecimalPlaces=0;
    }}

}
