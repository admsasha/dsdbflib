#ifndef DSDBFSTRUCT_H
#define DSDBFSTRUCT_H

#include <string>
#include <vector>
#include <string>
#include <map>
#include <stdint.h>

namespace DSDBFLib {

#pragma pack(push,1)
struct dbHeader {
    uint8_t signature;          // 0	0x00	1	Сигнатура.
    uint8_t lastUpdateYear;         // 1	0x01	3	Дата последней модификации в виде ГГММДД
    uint8_t lastUpdateMonth;        //
    uint8_t lastUpdateDay;          //
    uint32_t records;           // 4	0x04	4	Число записей в базе
    uint16_t lengthHeader;      // 8	0x08	2   Размер заголовка в байтах
    uint16_t lengthRecord;      // 10	0x0A	2	Длина одной записи
    uint16_t reserve1;          // 12	0x0C	2	Зарезервировано (всегда 0)
    uint8_t  flagTransaction;   // 14	0x0E	1	Флаг, указывающий на наличие незавершенной транзакции dBASE IV
    uint8_t  flagCrypto;        // 15	0x0F	1	Флаг шифрования таблицы dBASE IV
    uint32_t reserve2;          // 16	0x10	12	Зарезервированная область для многопользовательского использования
    uint32_t reserve3;          //
    uint32_t reserve4;          //
    uint8_t  flagIndMdx;        // 28	0x1C	1	Флаг наличия индексного MDX-файла
    uint8_t  codepage;          // 29	0x1D	1	Идентификатор кодовой страницы файла (dBASE IV, Visual FoxPro, XBase). См. табл 9.
    uint16_t reserve5;          // 30	0x1E	2	Зарезервировано (всегда 0)
    // 32	0x20	32	Только в dBASE 7. Идентификатор языкового драйвера. См. табл 10.
    // 64	0x40	4	Только в dBASE 7. Зарезервировано
};

struct dbField {
    char name[11];                      // 0	0x00	11	Имя поля. Для dBASE 7(32)
    char type;                          // 11	0x0B	1	Тип поля.
    uint32_t reserve1;                  // 12	0x0C	4	Зарезервировано
    uint8_t length;                     // 16	0x10	1	Полная длина поля
    uint8_t numberOfDecimalPlaces;      // 17	0x11	1	Число десятичных разрядов; для типа C - второй байт длины поля;
    uint8_t fieldFlags;                 // 18   0x12    1   Field flags
    uint32_t nextAutoIncrement;         //          	4	Для автоинкрементного поля - следующее значение, для прочих полей - 0 (только для dBASE 7)
    uint8_t flagMDX;                    //          	1	Флаг тэга файла MDX (только в dBASE IV)
    uint8_t reserve2[8];                //      	 	13	Зарезервировано (всегда 0)

    dbField(): name{""},type(0),reserve1(0),length(0),
        numberOfDecimalPlaces(0),nextAutoIncrement(0),
        flagMDX(0),reserve2{0}{}
};

enum class FIELD_TYPE : uint8_t {
    C = 0x43,       // Character
    Y = 0x59,       // Currency
    N = 0x4E,       // Numeric
    F = 0x46,       // Float
    D = 0x44,       // Date
    T = 0x54,       // DateTime
    B = 0x42,       // Binary
    I = 0x49,       // Integer
    L = 0x4C,       // Logical
    G = 0x47,       // General
    M = 0x4D,       // Memo
    P = 0x50,       // Picture
    AUTOINCREMENT = 0x2B,   // Autoincrement (dBase Level 7)
    O = 0x4F,               // Double (dBase Level 7)
    TIMESTAMP = 0x40        // Timestamp (dBase Level 7)
};

struct dbRecord {
    char type;
    std::map<std::string,std::string> record;
    dbRecord() : type(0x00){}
};
#pragma pack(pop)

class DSDBFStruct
{
public:
    DSDBFStruct();
};


}

#endif // DSDBFSTRUCT_H
