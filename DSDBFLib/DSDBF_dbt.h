#ifndef DSDBF_DBT_H
#define DSDBF_DBT_H

#include <string>
#include <fstream>

namespace DSDBFLib {

class DSDBF_dbt {
    public:
        DSDBF_dbt();
        bool open(const std::string &filename);
        void close();

        std::string readBlock(size_t block);

    private:
        std::string m_filename;
        std::ifstream m_fileHeader;

        int m_NextFreeBlock;
        int m_sizeBlock;

};

}

#endif // DSDBF_DBT_H
