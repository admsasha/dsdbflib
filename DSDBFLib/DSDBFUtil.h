#ifndef DSDBFUTIL_H
#define DSDBFUTIL_H

#include <string>
#include "DSDBFStruct.h"

namespace DSDBFLib {

class DSDBFUtil {
    public:
        DSDBFUtil();
        static void copyFile(std::string filename_origin, std::string filename_destination);

        static uint8_t readUint8(std::ifstream *stream, size_t position);
        static uint16_t readUint16(std::ifstream *stream, size_t position);
        static uint32_t readUint32(std::ifstream *stream, size_t position);

        static void writeUint16(std::ofstream *stream, size_t position, uint16_t value);

        static void writeData(char *source, char *dest, int pos, int length);

        static bool convertStringToBinary(char *dest, std::string str, dbField field);
        static std::string convertBinaryToString(char *source, dbField field);
        static void normalizeLengthField(FIELD_TYPE fieldType, uint8_t *length, uint8_t *numberOfDecimalPlaces);
};

}
#endif // DSDBFUTIL_H
