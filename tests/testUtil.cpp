#include <gtest/gtest.h>

#include <string.h>
#include <fstream>

#include "DSDBFLib/DSDBFUtil.h"

namespace DSDBFLib {

TEST( testDSDBFUtil, readUint ) {
    std::ifstream  ifs;
	ifs.open ("test.txt", std::ifstream::in | std::ifstream::binary);
    EXPECT_EQ (ifs.is_open(),true);

	EXPECT_EQ(DSDBFUtil::readUint8(&ifs,0x00),244);
	EXPECT_EQ(DSDBFUtil::readUint16(&ifs,0x01),11035);
	EXPECT_EQ(DSDBFUtil::readUint32(&ifs,0x06),1295854877);

	ifs.close();
}

TEST( testDSDBFUtil, convertStringToBinary_Field_C ) {
	dbField fieldC;
	fieldC.type=0x43;
	fieldC.length=10;
	char checkC[] = {0x74,0x65,0x73,0x74,0x20,0x20,0x20,0x20,0x20,0x20};
	
    char * buffer = new char [fieldC.length+1];
    bool rez=DSDBFUtil::convertStringToBinary(buffer,"test",fieldC);
    EXPECT_EQ (rez,true);
	EXPECT_EQ(strncmp(buffer,checkC,fieldC.length),0);
    delete[] buffer;
}

TEST( testDSDBFUtil, convertStringToBinary_Field_N ) {
	dbField fieldN;
	fieldN.type=0x4E;
	fieldN.length=10;
	fieldN.numberOfDecimalPlaces=2;
	
	char checkN[] = {0x20,0x20,0x20,0x20,0x31,0x32,0x33,0x2e,0x33,0x34};
	char checkN2[] = {0x20,0x20,0x20,0x20,0x31,0x32,0x33,0x2e,0x30,0x30};

	
        {   // checkN
            char * buffer = new char [fieldN.length+1];
	    bool rez=DSDBFUtil::convertStringToBinary(buffer,"123.34",fieldN);
	    EXPECT_EQ (rez,true);
            EXPECT_EQ(strncmp(buffer,checkN,fieldN.length),0);
	    delete[] buffer;
	}

        {   // checkN2
            char * buffer = new char [fieldN.length+1];
	    bool rez=DSDBFUtil::convertStringToBinary(buffer,"123",fieldN);
	    EXPECT_EQ (rez,true);
            EXPECT_EQ(strncmp(buffer,checkN2,fieldN.length),0);
	    delete[] buffer;
	}

}

TEST( testDSDBFUtil, convertBinaryToString_Field_Y ) {
    dbField fieldY;
    fieldY.type=(char)FIELD_TYPE::Y;
    fieldY.length=8;
    fieldY.numberOfDecimalPlaces=4;

    unsigned char checkY[] = {0x90,0xE5,0xFF,0xBF,0x2F,0x68,0x95,0x0D};
    std::string checkResult = "97880304831692.1232";
    EXPECT_EQ(DSDBFUtil::convertBinaryToString(reinterpret_cast<char*>(checkY), fieldY),checkResult);
}


TEST( testDSDBFUtil, normalizeLengthField ) {
	uint8_t length=0;
	uint8_t numberOfDecimalPlaces=0;

	DSDBFUtil::normalizeLengthField(FIELD_TYPE::Y,&length,&numberOfDecimalPlaces);
	EXPECT_EQ(length,8);
	EXPECT_EQ(numberOfDecimalPlaces,4);

	char type=0x4C;
	DSDBFUtil::normalizeLengthField((FIELD_TYPE)type,&length,&numberOfDecimalPlaces);
	EXPECT_EQ(length,1);
	EXPECT_EQ(numberOfDecimalPlaces,0);

}


}
