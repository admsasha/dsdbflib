#include <gtest/gtest.h>

#include <string.h>
#include <fstream>

#include "DSDBFLib/DSDBFLib.h"

TEST( testDSDBFLibEditor, testSetFileType ) {
    DSDBFLib::DSDBFLib dbf("test_editor.dbf");
    EXPECT_EQ(dbf.open(),true);

    dbf.setFileType(0x31);
    EXPECT_EQ(dbf.getFileType(),0x31);

    dbf.setFileType(0x30);
    EXPECT_EQ(dbf.getFileType(),0x30);

    dbf.close();
}

TEST( testDSDBFLibEditor, testSetTimeLastUpdate ) {
    DSDBFLib::DSDBFLib dbf("test_editor.dbf");
    EXPECT_EQ(dbf.open(),true);

    dbf.setTimeLastUpdate(1,1,2017);
    EXPECT_EQ(dbf.getTimeLastUpdate(),"01.01.2017");

    dbf.setTimeLastUpdate(11,12,1993);
    EXPECT_EQ(dbf.getTimeLastUpdate(),"11.12.1993");

    dbf.close();
}

TEST( testDSDBFLibEditor, testSetCodepage ) {
    DSDBFLib::DSDBFLib dbf("test_editor.dbf");
    EXPECT_EQ(dbf.open(),true);

    EXPECT_EQ(dbf.setCodepage(866),true);
    EXPECT_EQ(dbf.getCodepage(),866);

    EXPECT_EQ(dbf.setCodepage((uint8_t)0xC9),true);
    EXPECT_EQ(dbf.getCodepage(),1251);

    dbf.close();
}

TEST( testDSDBFLibEditor, testCreateTable ) {
    std::string tmpFilename = std::tmpnam(nullptr);
    remove(tmpFilename.c_str());

    DSDBFLib::DSDBFLib dbf(tmpFilename);
    EXPECT_EQ(dbf.createDbfFile(0x03),true);
    remove(tmpFilename.c_str());
}

TEST( testDSDBFLibEditor, testCreateField ) {
    std::string tmpFilename = std::tmpnam(nullptr);
    remove(tmpFilename.c_str());

    DSDBFLib::DSDBFLib dbf(tmpFilename);
    EXPECT_EQ(dbf.createDbfFile(0x03),true);

    dbf.insertField("F1",DSDBFLib::FIELD_TYPE::Y);
    dbf.insertField("F2",DSDBFLib::FIELD_TYPE::N,10,0);
    dbf.insertField("F3",DSDBFLib::FIELD_TYPE::L);

    EXPECT_EQ(dbf.open(),true);

    std::vector<DSDBFLib::dbField> fields = dbf.getFields();
    for (DSDBFLib::dbField field : fields){
        if (strncmp(field.name,"F1",2)==0){
            EXPECT_EQ(field.length,8);
            EXPECT_EQ(field.numberOfDecimalPlaces,4);
            EXPECT_EQ(field.type,(char)DSDBFLib::FIELD_TYPE::Y);
        }else if (strncmp(field.name,"F2",2)==0){
            EXPECT_EQ(field.length,10);
            EXPECT_EQ(field.numberOfDecimalPlaces,0);
            EXPECT_EQ(field.type,(char)DSDBFLib::FIELD_TYPE::N);
        }else if (strncmp(field.name,"F3",2)==0){
            EXPECT_EQ(field.length,1);
            EXPECT_EQ(field.numberOfDecimalPlaces,0);
            EXPECT_EQ(field.type,(char)DSDBFLib::FIELD_TYPE::L);
        }else{
            FAIL();
        }
    }

    dbf.close();

    remove(tmpFilename.c_str());
}
