#include <gtest/gtest.h>

#include <string.h>
#include <fstream>
#include <direct.h>

#include "DSDBFLib/DSDBFLib.h"

int setChDir(char* arg0) {
   char *currentPath;
   size_t found;
    
   std::string path(arg0);
   
   found=path.find_last_of("/\\");

   currentPath = (char*) calloc (path.length(),sizeof(char));
   strcpy(currentPath,path.substr(0,found).c_str());

   int result = chdir(currentPath);

   free(currentPath);
   
   return result;
}

std::string loadFile(const std::string &filename){
	std::ifstream file;
    file.open (filename, std::ifstream::binary);
    EXPECT_EQ(file.is_open(),true);

    file.seekg (0, file.end);
    int length = file.tellg();
    file.seekg (0, file.beg);

	char * buffer = new char [length];
	file.read (buffer,length);
	std::string result = std::string(buffer,length);
	delete[] buffer;

	return result;
}
TEST( testDSDBFLib, TestStructDbField ) {
    EXPECT_EQ(sizeof(DSDBFLib::dbField),32);
}

TEST( testDSDBFLib, TestOpenExist ) {
    DSDBFLib::DSDBFLib dbf("test.dbf");
    EXPECT_EQ(dbf.open(),true);
	dbf.close();
}

TEST( testDSDBFLib, TestOpenNotExist ) {
    DSDBFLib::DSDBFLib dbf("non_exist_file.dbf");
    EXPECT_EQ(dbf.open(),false);
	dbf.close();
}

TEST( testDSDBFLib, TestGetValue ) {
    DSDBFLib::DSDBFLib dbf("test2.dbf");
    EXPECT_EQ(dbf.open(),true);

	DSDBFLib::dbRecord record = dbf.getRecord(0);
	EXPECT_EQ(record.record["FIELD1"],"a ");
	EXPECT_EQ(record.record["FIELD2"],"20170906");
	EXPECT_EQ(record.record["FIELD3"],"     11.00");
	EXPECT_EQ(record.record["FIELD4"],"T");
	EXPECT_EQ(record.record["FIELD5"],"e                ");
	EXPECT_EQ(record.record["FIELD6"],"10.2300");
	EXPECT_EQ(record.record["FIELD7"],"      1.11");



	DSDBFLib::dbRecord record2 = dbf.getRecord(2);
	
	float testf=1234567890.123500;
	EXPECT_EQ(stof(record2.record["FIELD6"]),testf);

	dbf.close();
}

TEST( testDSDBFLib, TestCheckStruct ) {
    DSDBFLib::DSDBFLib dbf("test2.dbf");
    EXPECT_EQ(dbf.open(),true);

	EXPECT_EQ(dbf.getNumFields(),7);
	EXPECT_EQ(dbf.getNumRecords(),4);
	
	dbf.close();
}

TEST( testDSDBFLib, TestCheckFields ) {
    DSDBFLib::DSDBFLib dbf("test2.dbf");
    EXPECT_EQ(dbf.open(),true);

	std::vector<DSDBFLib::dbField> fields = dbf.getFields();
	for (DSDBFLib::dbField field : fields){
		if (strcmp(field.name,"FIELD1")==0){
			EXPECT_EQ(field.type,0x43);
			EXPECT_EQ(field.length,2);
			EXPECT_EQ(field.numberOfDecimalPlaces,0);
		}
		if (strcmp(field.name,"FIELD6")==0){
			EXPECT_EQ(field.type,0x59);
			EXPECT_EQ(field.length,8);
			EXPECT_EQ(field.numberOfDecimalPlaces,4);
		}
	}

	dbf.close();
}

TEST( testDSDBFLib, TestGetMemo ) {
    DSDBFLib::DSDBFLib dbf("test4.dbf");
    EXPECT_EQ(dbf.open(),true);
	
	DSDBFLib::dbRecord record = dbf.getRecord(1);
	EXPECT_EQ(record.record["F2"],"ffff");

	dbf.close();
}

TEST( testDSDBFLib, TestCheckIncrement ) {
    DSDBFLib::DSDBFLib dbf("test4.dbf");
    EXPECT_EQ(dbf.open(),true);
	
	DSDBFLib::dbRecord record = dbf.getRecord(0);
	EXPECT_EQ(record.record["F3"],"1");

	record = dbf.getRecord(1);
	EXPECT_EQ(record.record["F3"],"2");

	record = dbf.getRecord(2);
	EXPECT_EQ(record.record["F3"],"3");

	dbf.close();
}


TEST( testDSDBFLib, TestCheckTypeP ) {
    DSDBFLib::DSDBFLib dbf("testP.dbf");
    EXPECT_EQ(dbf.open(),true);
	
	DSDBFLib::dbRecord record = dbf.getRecord(0);
	std::string file1bmp = loadFile("test1.bmp");
	EXPECT_EQ(record.record["F3"],file1bmp);
	
	record = dbf.getRecord(1);
	std::string file2bmp = loadFile("test2.bmp");
	EXPECT_EQ(record.record["F3"],file2bmp);
	
	record = dbf.getRecord(2);
	std::string file3bmp = loadFile("test3.bmp");
	EXPECT_EQ(record.record["F3"],file3bmp);


	dbf.close();
}

TEST( testDSDBFLib, TestCheckTypeT ) {
    DSDBFLib::DSDBFLib dbf("test4.dbf");
    EXPECT_EQ(dbf.open(),true);
	
	DSDBFLib::dbRecord record = dbf.getRecord(0);
	EXPECT_EQ(record.record["F1"],"16.06.2017");

	dbf.close();
}

/*
TEST( testDSDBFLib, TestCheckDbase7 ) {
    DSDBFLib::DSDBFLib dbf("test_dbase7.dbf");
    EXPECT_EQ(dbf.open(),true);

	std::string file1bmp = loadFile("test1.bmp");
	std::string file2bmp = loadFile("test2.bmp");

	DSDBFLib::dbRecord record = dbf.getRecord(0);
	EXPECT_EQ(record.record["F1"],file1bmp);
	EXPECT_EQ(record.record["F2"],file2bmp);

	dbf.close();
}
*/

int main( int argc, char **argv ) {
    ::testing::InitGoogleTest( &argc, argv );
    setChDir(argv[0]);
    return RUN_ALL_TESTS( );
}
