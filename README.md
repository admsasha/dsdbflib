### Description ###

Simple library for working with dbf files


Support field type: C, Y, N, F, D, T, B, I, L, M, G, C, P, +, O, @

Support database:  dBASE III, dBASE IV, dBASE V, FoxPro, Visual FoxPro, FoxBASE+

### Sample ###

	#include "DSDBFLib/DSDBFLib.h"
	#include <iostream>
	#include <iomanip>
	
	
	int main(int argc, char *argv[]){
    	DSDBFLib::DSDBFLib dbf("test.dbf");
    	dbf.open();

    	std::cout << "sign: 0x" << std::hex << std::setw(2) << std::setfill('0') << int(dbf.getFileType()) << std::endl;
    	std::cout << "type: " << dbf.getFileTypeStr() << std::endl;
    	std::cout << "codepage: " << std::dec << dbf.getCodepage() << std::endl;
    	std::cout << "TimeLastUpdate: " << std::dec  << dbf.getTimeLastUpdate() << std::endl;
    	std::cout << "records: " << std::dec << dbf.getNumRecords() << std::endl;

    	std::cout << std::endl;

    	std::vector<DSDBFLib::dbField> fields = dbf.getFields();
    	for (DSDBFLib::dbField field : fields){
        	std::cout << "name: " << field.name << "  ";
            std::cout << "type:" << field.type << "  " ;
        	std::cout << "length: " << std::dec << field.length << "  ";
        	std::cout << "numberOfDecimalPlaces: " << std::dec << field.numberOfDecimalPlaces << "  ";
        	std::cout << std::endl;
    	}


    	std::cout << std::endl;

    	for (size_t i=0;i<dbf.getNumRecords();i++){
        	DSDBFLib::dbRecord record = dbf.getRecord(i);
        	for (DSDBFLib::dbField field : fields){
            	std::cout << record.record[std::string(field.name)] << "|";
        	}
        	std::cout << std::endl;
    	}

    	return 0;
	}


### Who do I talk to? ###

email: dik@inbox.ru